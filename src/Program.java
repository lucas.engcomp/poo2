import entities.Product;

import java.util.Locale;
import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Locale.setDefault(Locale.US);

        Product product = new Product();

        System.out.println("Enter product data: ");
        System.out.println("Enter name product ");
        product.name = scanner.nextLine();
        System.out.println("Enter price product: ");
        product.price = scanner.nextDouble();
        System.out.println("Enter quantity product: ");
        product.quantity = scanner.nextInt();

        System.out.println(product);

        System.out.println();

        System.out.println("Enter the number of products to be added in stock: ");
        int quantityAdd = scanner.nextInt();
        product.addProducts(quantityAdd);
        System.out.println(product);

        System.out.println("Enter the number of products to be remove in stock: ");
        int quantityRemove = scanner.nextInt();
        product.removeProducts(quantityRemove);
        System.out.println(product);

        scanner.close();
    }
}
